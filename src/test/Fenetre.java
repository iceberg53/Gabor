package test;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;


public class Fenetre extends JFrame implements ActionListener {
	JButton choisir = new JButton("Open Image");
	JButton filtrer = new JButton("Filter");
	JButton enregistrer = new JButton("Save");
	JLabel label = new JLabel();
	BufferedImage image;
	BufferedImage imgS;
	BufferedImage imgF;
	BufferedImage imageFiltree;
	Panneau panel1 = new Panneau(); 
	Panneau panel2 = new Panneau();
	JScrollPane scp1 = new JScrollPane(panel1);
	JScrollPane scp2 = new JScrollPane(panel2);
	JPanel p2 = new JPanel();
	JPanel p1 = new JPanel();
	JPanel sp1 = new JPanel();
	JPanel sp2 = new JPanel();
	File savedImage;
	
	public Fenetre(String titre){
		this.setTitle(titre);
		this.setLocationRelativeTo(null);
		this.setSize(600, 500);
		this.setBackground(Color.black);
		
		GridLayout gl1 = new GridLayout(1,2);
		gl1.setHgap(5);
		p1.setLayout(gl1);
		p1.add(sp1);
		p1.add(sp2);
		sp1.setLayout(new BoxLayout(sp1, BoxLayout.PAGE_AXIS));
		sp1.add(choisir);
		sp1.add(filtrer);
		sp2.setLayout(new BoxLayout(sp2, BoxLayout.LINE_AXIS));
		sp2.add(enregistrer);
		enregistrer.addActionListener(this);
		choisir.addActionListener(this);
		filtrer.addActionListener(this);
		
		GridLayout gl2 = new GridLayout(1,2);
		gl2.setHgap(5);
		p2.setLayout(gl2);
		p2.add(scp1);
		p2.add(scp2);
		
		try {
        	imgS = ImageIO.read(new File("imageSource.png"));
        	imgF = ImageIO.read(new File("imageFiltree.png"));
        	panel1.setImage(imgS);
        	panel2.setImage(imgF);
    		panel1.setPreferredSize(new Dimension(imgS.getHeight(),imgS.getWidth()));
    		panel2.setPreferredSize(new Dimension(imgF.getHeight(),imgF.getWidth()));

        	
			
		} catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
		this.getContentPane().add(p1,BorderLayout.NORTH);
		this.getContentPane().add(p2,BorderLayout.CENTER);
	    	
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		setVisible(true);
	}
	
	
	public void actionPerformed(ActionEvent argAction) {
		if(argAction.getSource() == choisir){
			JFileChooser imageChoisie = new JFileChooser();
			imageChoisie.setAcceptAllFileFilterUsed(false);
			String extensions[]={"bmp", "gif", "jpg", "jpeg", "png"};
			FileFilter imagesFilter = new FileNameExtensionFilter("bmp,gif,jpg,jpeg, png",extensions);
            imageChoisie.addChoosableFileFilter(imagesFilter);
            if (imageChoisie.showOpenDialog(this)== JFileChooser.APPROVE_OPTION)
            {
            	try {
                	image = ImageIO.read(new File(imageChoisie.getSelectedFile().getAbsolutePath()));
                	imgF = ImageIO.read(new File("imageFiltree.png"));
                	panel1.setImage(image);
                	panel2.setImage(imgF);
            		panel1.setPreferredSize(new Dimension(image.getHeight(),image.getWidth()));
            		panel2.setPreferredSize(new Dimension(imgF.getHeight(),imgF.getWidth()));

        			
        		} catch (IOException e) {
        			// TODO: handle exception
        			e.printStackTrace();
        		}
            }
            
            
            
 
		}
		if(argAction.getSource() == filtrer){
			Filter filtre = new Filter(image);
        	imageFiltree = filtre.getResult();
        	panel2.setImage(imageFiltree);
    		panel2.setPreferredSize(new Dimension(imageFiltree.getHeight(),imageFiltree.getWidth()));

		}
		if(argAction.getSource() == enregistrer){
			JFileChooser saveImage = new JFileChooser();
			
            if (saveImage.showSaveDialog(null)== JFileChooser.APPROVE_OPTION)
            {
            File fichier = new File(saveImage.getSelectedFile().getAbsolutePath()+".JPG");
			panel2.saveImage(fichier);
            }
			
		}
	}
}
