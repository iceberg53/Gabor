package test;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class Panneau extends JPanel {
	private BufferedImage image;
	
	public Panneau(){
		
	}
	
	public void paintComponent(Graphics g){
		
			
			g.drawImage(image,0, 0, this.getWidth(), this.getHeight(),this);
			repaint();
	}
	
	
	public void setImage(BufferedImage image)
	{
		this.image = image;
		repaint();
	}
	
	public BufferedImage getImage(){
		return image;
	}
	
	protected void saveImage(File imageFile)
	{
		String format ="JPG";
		BufferedImage image = this.getImage();
		try {
			ImageIO.write(image, format, imageFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
