package test;

import ij.ImagePlus;
import ij.ImageStack;
import ij.io.Opener;
import ij.plugin.filter.Convolver;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;

import java.awt.image.BufferedImage;


public class Filter {
	double F=3.0;
    double sigma_x=4,
            sigma_y=16;
    private FloatProcessor filter;
    private final ImageStack kernels;
    private final ImageStack is;
    private final float[] kernel;
    private final ImagePlus ip_kernels;
    private final ImagePlus result;
    
    
    public Filter(BufferedImage image ){
        
    	ImagePlus originalImage = new ImagePlus("Image Originale", image);
    	 
        originalImage=new ImagePlus(originalImage.getTitle(),
                            originalImage.getProcessor().convertToFloat());
        
        int width=originalImage.getWidth(),
            height=originalImage.getHeight();
        
        ImageProcessor ip=originalImage.getProcessor().duplicate();
        
        double sigma_x2=sigma_x*sigma_x;
        double sigma_y2=sigma_y*sigma_y;
        
        
        int filterSizeX=19,
            filterSizeY=19;
        
        int middleX= (int) Math.round((filterSizeX)/2);
        int middleY= (int) Math.round((filterSizeY)/2);
        
        double theta=Math.PI/(double)6;
        
        //La pile d'images
        is = new ImageStack(width, height);
        kernels = new ImageStack(filterSizeX, filterSizeY);
        filter = new FloatProcessor(filterSizeX, filterSizeY);
        
        for(int x=-middleX;x<=middleX;x++){
            for(int y=-middleY;y<=middleY;y++){
                
                double xPrime= (double)x * Math.cos(theta)+ (double)y * Math.sin(theta);
                double yPrime= (double)y * Math.cos(theta)- (double)x * Math.sin(theta);
                
                double a= 1.0/(2.0*Math.PI*sigma_x*sigma_y) * Math.exp(-0.5 *(xPrime*xPrime/sigma_x2 + yPrime*yPrime/sigma_y2));
                double c=a * Math.cos(2.0 * Math.PI *(F *xPrime)/filterSizeX);
                filter.setf(x+middleX, y+middleY, (float)(a*c) );
            }
        }
        kernels.addSlice("kernel angle = " + theta, filter);
        kernel = (float[]) kernels.getProcessor(1).getPixels();
        
        ip_kernels = new ImagePlus("kernels", kernels);
        
        Convolver c=new Convolver();
        c.convolveFloat(ip, kernel, filterSizeX, filterSizeY);      
 
        is.addSlice("gabor angle = ", ip);
        
        result= new ImagePlus("",is);
        
        
    }
    
    public BufferedImage getResult()
	{
    	BufferedImage imageFiltree;
    	imageFiltree = result.getBufferedImage();
		return imageFiltree;
	}
    
   
}